# TD_Dracula

## Credits
This theme is based upon the dracula rounded community theme. That theme can be downloaded [here](https://github.com/AethanFoot/leftwm-theme-dracula-rounded.git)

The bluetooth status and toggle module for polybar is an edited version of [system-bluetooth-bluetoothctl](https://github.com/polybar/polybar-scripts/tree/master/polybar-scripts/system-bluetooth-bluetoothctl)

The bluetooth rofi menu is from [rofi-bluetooth](https://github.com/nickclyde/rofi-bluetooth)

The network rofi menu is from [rofi-network-manager](https://github.com/P3rf/rofi-network-manager)

## Packages

```
DE          : Arch
WM          : Leftwm
terminal    : Alacritty/st
colorscheme : TD_Dark
bar         : Polybar
launcher    : Rofi
```

## Dependencies

Everythings but leftwm is optional. It will work without the rest, but functionallity can be lost.

- [leftwm-git](https://github.com/leftwm/leftwm)
- [ibhagwan picom](https://github.com/ibhagwan/picom)
- [polybar](https://github.com/polybar/polybar)
- [saucecodepro nerd font](https://github.com/ryanoasis/nerd-fonts)
- [rofi](https://github.com/davatorium/rofi)

## Installation

1. Install all required dependencies

2. Clone the repository

```BASH
git clone https://codeberg.org/TotalDarkness/LeftWM-Themes
```

3. Make a copy of this theme in your themes folder

```BASH
cp -r ./LeftWM-Themes/TD_Dark ~/.config/leftwm/themes
```

4. Remove the symlink to your current theme if set

```BASH
rm ~/.config/leftwm/themes/current
```

5. Set this as your current theme

```BASH
ln -s ~/.config/leftwm/themes/TD_Dark ~/.config/leftwm/themes/current
```

6. Restart your window manager

```Default shortcut
$MOD + Shift + r
```

## Configuration

There are two launchers that can be used, list and touch. You can switch between these by linking either to the file launcher.rasi e.g.:
```BASH
ln -s list_launcher.rasi launcher.rasi
```
