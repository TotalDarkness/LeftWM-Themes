# TotalDarkness LeftWM Themes

## Themes
- TD_Dark

## Installation

1. Install all required dependencies

2. Clone the repository

```BASH
git clone https://codeberg.org/TotalDarkness/LeftWM-Themes
```

3. Select a theme and make a copy of the theme into your themes folder

```BASH
cp -r ./LeftWM-Themes/THEME ~/.config/leftwm/themes
```

4. Remove the symlink to your current theme if set

```BASH
rm ~/.config/leftwm/themes/current
```

5. Set this as your current theme

```BASH
ln -s ~/.config/leftwm/themes/THEME ~/.config/leftwm/themes/current
```

6. Restart your window manager

```Default shortcut
$MOD + Shift + r
```